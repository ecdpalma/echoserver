'''
Created on 17/08/2011

@author: ecdpalma
'''
# A setup script showing advanced features.
#
# Note that for the NT service to build correctly, you need at least
# win32all build 161, for the COM samples, you need build 163.
# Requires wxPython, and Tim Golden's WMI module.

# Note: WMI is probably NOT a good example for demonstrating how to
# include a pywin32 typelib wrapper into the exe: wmi uses different
# typelib versions on win2k and winXP.  The resulting exe will only
# run on the same windows version as the one used to build the exe.
# So, the newest version of wmi.py doesn't use any typelib anymore.

from distutils.core import setup
from subprocess import PIPE, Popen, call
import py2exe
import sys
import os

local_dir = os.path.dirname(sys.argv[0])

# If run without args, build executables, in quiet mode.
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
    sys.argv.append("-q")

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        # for the versioninfo resources
        self.version = "0.1.0"
        self.company_name = "PIT"
        self.copyright = "Copyright 2011"
        self.name = "Name"

################################################################
# A program using wxPython

# The manifest will be inserted as resource into test_wx.exe.  This
# gives the controls the Windows XP appearance (if run on XP ;-)
#
# Another option would be to store it in a file named
# test_wx.exe.manifest, and copy it with the data_files option into
# the dist-dir.
#
################################################################
# a NT service, modules is required
myservice = Target(
    # used for the versioninfo resource
    description="Echo Server",
    # what to build.  For a service, the module name (not the
    # filename) must be specified!
    modules=["echoservice"],
    cmdline_style='pywin32'
    )

################################################################
# COM pulls in a lot of stuff which we don't want or need.

excludes = ["pywin", "pywin.debugger", "pywin.debugger.dbgcon",
            "pywin.dialogs", "pywin.dialogs.list", "tcl", "Tkconstants", "Tkinter"]

dll_excludes = ['w9xpopen.exe']

setup(
    options={"py2exe": {"typelibs":
                          # typelib for WMI
                          [('{565783C6-CB41-11D1-8B02-00600806D9B6}', 0, 1, 2)],
                          # create a compressed zip archive
                          "compressed": 1,
                          "optimize": 2,
                          "bundle_files": 1,
                          "excludes": excludes,
                          "dll_excludes": dll_excludes}},
    # The lib directory contains everything except the executables and the python dll.
    # Can include a subdirectory name.
    zipfile=None,
    data_files=[(".", ["echoservice.conf"])],
    service=[myservice],
    )

cmd = os.environ['ProgramFiles'] + "\\Windows Installer XML v3.5\\bin\\candle.exe -nologo " + local_dir + "echoserver.xml -out " + local_dir + "echoserver.wixobj"
print cmd
call(cmd)
cmd = os.environ['ProgramFiles'] + "\\Windows Installer XML v3.5\\bin\\light.exe -nologo " + local_dir + "echoserver.wixobj -out " + local_dir + "EchoServer.msi"
print cmd
call(cmd)
