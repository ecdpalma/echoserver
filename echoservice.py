'''
Created on 13/10/2011

@author: ecdpalma
'''

import win32serviceutil
import win32event
import win32service
import thread
import os
import sys

from echoserver import EchoServer

local_dir = os.path.dirname(sys.argv[0])
current_path = os.path.normpath(os.path.join(os.getcwd(), local_dir))

class EchoService(win32serviceutil.ServiceFramework):
    _svc_name_ = "EchoServer"
    _svc_display_name_ = "Echo Server"

    def SvcDoRun(self):
        echo_server = EchoServer()
        echo_server.run()

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        self.ReportServiceStatus(win32service.SERVICE_STOPPED)
        # very important for use with py2exe
        # otherwise the Service Controller never knows that it is stopped !

if __name__ == '__main__':
    if (len(sys.argv) > 1):
        win32serviceutil.HandleCommandLine(EchoService)
    else:
        echo_server = EchoServer()
        echo_server.run()
