'''
Created on 13/10/2011

@author: ecdpalma
'''
import os
import logging


local_dir = os.path.dirname(__file__)
current_path = os.path.normpath(os.path.join(os.getcwd(), local_dir))

# create logger
logger = logging.getLogger('echo_server_logger')


def set_log_file(filename):
    # create console handler and set level to debug
    ch = logging.FileHandler(filename)
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

def set_log_level(level):
    logger.setLevel(eval(level))
