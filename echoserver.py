'''
Created on 10/13/2011

@author: ecdpalma
'''
import sys
import logging
import os
from socket import AF_INET, SOCK_STREAM, socket
import threading
import thread
import ConfigParser

from logger import logger, set_log_file, set_log_level

local_dir = os.path.dirname(sys.argv[0])
current_path = os.path.normpath(os.path.join(os.getcwd(), local_dir))

config = ConfigParser.RawConfigParser()
config.read(os.path.join(local_dir, "echoservice.conf"))

set_log_level(config.get("global", "log_level"))
set_log_file(config.get("global", "log_file"))

class EchoServer():
    def __init__(self):
        self.host = config.get("global", "host")
        self.port = config.getint("global", "port")
        self.serversock = socket(AF_INET, SOCK_STREAM)
        self.serversock.bind((self.host, self.port))
        self.serversock.listen(2)

    def handler(self, clientsock, addr):
        while True:
            logger.log(logging.DEBUG, "Receiving data")
            data = clientsock.recv(1024)
            logger.log(logging.DEBUG, "Received " + data)
            if not data:
                break
            clientsock.send(data)
        clientsock.close()

    def run(self):
        logger.log(logging.INFO, "Waiting connections")
        while True:
            clientsock, addr = self.serversock.accept()
            logger.log(logging.INFO, "Connection from " + str(addr[0]))
            thread.start_new_thread(self.handler, (clientsock, addr))
